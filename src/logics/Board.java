package logics;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Logical  representation of a chess board with its pieces, moves, etc.
public class Board {

    public int[][] fields;
    public List<Move> moves;
    public List<Move> movesAllSimple;
    public boolean gotMovesSimple;
    public boolean gotMovesComplete;
    public boolean gotAllMovesSimple;
    public int turn;
    public int[] canCastle;
    public boolean[] hasCastled;
    public int[] kingX;
    public int[] kingY;
    public int estimation;
    public boolean gotEstimation;
    public int result;
    public int fiftyMovesHalfmoves;
    public int halfmoves;
    public int enPassant;

    public static final int PIECE_NONE = 0;
    public static final int PIECE_PAWN_WHITE = 1;
    public static final int PIECE_KNIGHT_WHITE = 2;
    public static final int PIECE_BISHOP_WHITE = 3;
    public static final int PIECE_ROOK_WHITE = 4;
    public static final int PIECE_QUEEN_WHITE = 5;
    public static final int PIECE_KING_WHITE = 6;
    public static final int PIECE_PAWN_BLACK = -1;
    public static final int PIECE_KNIGHT_BLACK = -2;
    public static final int PIECE_BISHOP_BLACK = -3;
    public static final int PIECE_ROOK_BLACK = -4;
    public static final int PIECE_QUEEN_BLACK = -5;
    public static final int PIECE_KING_BLACK = -6;

    public static final int PLAYER_WHITE = 0;
    public static final int PLAYER_BLACK = 1;
    public static final int PLAYER_BOTH = 2;

    public static final int RESULT_UNKNOWN = -2;
    public static final int RESULT_OPEN = -1;
    public static final int RESULT_WIN_WHITE = 0;
    public static final int RESULT_WIN_BLACK = 1;
    public static final int RESULT_TIE = 2;

    public Board() {

        this.fields = new int[8][8];
        this.canCastle = new int[2];
        this.hasCastled = new boolean[2];
        this.kingX = new int[2];
        this.kingY = new int[2];
        this.enPassant = -1;
        this.result = RESULT_UNKNOWN;
    }

    public Board(String fen) {

        this();
        this.setFen(fen);
    }

    public void start() {

        this.setFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    }

    public Board(Board toClone) {

        this.fields = new int[8][];

        for (int col = 0; col < 8; col++) {
            this.fields[col] = Arrays.copyOf(toClone.fields[col], 8);
        }

        this.canCastle = Arrays.copyOf(toClone.canCastle, 2);
        this.hasCastled = Arrays.copyOf(toClone.hasCastled, 2);
        this.kingX = Arrays.copyOf(toClone.kingX, 2);
        this.kingY = Arrays.copyOf(toClone.kingY, 2);

        this.enPassant = toClone.enPassant;
        this.turn = toClone.turn;
        this.fiftyMovesHalfmoves = toClone.fiftyMovesHalfmoves;
        this.halfmoves = toClone.halfmoves;
    }

    public int getNumPieces() {

        int res = 0;

        for (int[] col: this.fields) {
            for (int piece:col) {
                if (piece != PIECE_NONE) res ++;
            }
        }

        return res;
    }

    // Cached result calculation function
    public int getResult() {

        if (result == RESULT_UNKNOWN) {
            this.calculateResult();
        }

        return this.result;
    }

    // Result calculation: Check if game has a winner or is tied
    public void calculateResult() {

        if (this.getMoves().isEmpty()) {
            List<Move> movesEnemy = this.calculateMovesSimpleFor(this.enemy());
            for (Move mv : movesEnemy) {
                if (mv.capturedPiece == PIECE_KING_WHITE || mv.capturedPiece == PIECE_KING_BLACK) {
                    this.result = (this.turn == PLAYER_WHITE ? RESULT_WIN_BLACK : RESULT_WIN_WHITE);
                    return;
                }
            }
            this.result = RESULT_TIE;
            return;
        }

        if (this.fiftyMovesHalfmoves >= 100) {
            this.result = RESULT_TIE;
            return;
        }

        this.result = RESULT_OPEN;
    }

    // Cached moves calculation function
    public List<Move> getMoves(boolean complete) {

        if (this.moves == null) this.moves = new ArrayList<>();

        if (!this.gotMovesSimple) this.calculateMovesSimple();
        if (complete && !this.gotMovesComplete) this.refineMoves();

        return this.moves;
    }

    public List<Move> getMoves() {

        return this.getMoves(true);
    }

    public List<Move> getAllMovesSimple() {

        if (this.movesAllSimple == null) this.movesAllSimple = new ArrayList<>();

        if (!this.gotAllMovesSimple) this.calculateAllMovesSimple();

        return this.movesAllSimple;
    }

    public void calculateAllMovesSimple() {

        this.gotAllMovesSimple = true;
        this.movesAllSimple.clear();

        this.movesAllSimple = calculateMovesSimpleFor(PLAYER_BOTH);
    }

    //  Moves calculation: Get possible moves while omitting the steps that could be done in the moves refinement function
    public void calculateMovesSimple() {

        this.gotMovesSimple = true;
        this.moves.clear();

        this.moves = calculateMovesSimpleFor(this.turn);
    }

    public List<Move> calculateMovesSimpleFor(int player) {

        List<Move> res = new ArrayList<>();

        // TODO: Adjust actual calculations instead of using recursive call for both players
        if (player == PLAYER_BOTH) {
            res.addAll(calculateMovesSimpleFor(PLAYER_WHITE));
            res.addAll(calculateMovesSimpleFor(PLAYER_BLACK));
            return res;
        }

        int direction;
        int pawnBaseline;
        int aimX;
        int aimY;
        int distance;

        // Go through fields
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {

                // Only calculate moves of pieces of requested player
                if (player != PLAYER_BOTH && !this.isPieceOfPlayer(x, y, player)) continue;

                // Check which piece was found and calculate its possible moves accordingly
                switch (this.fields[x][y]) {

                    case PIECE_PAWN_WHITE:
                    case PIECE_PAWN_BLACK:
                        direction = (this.fields[x][y] == PIECE_PAWN_WHITE ? 1 : -1);
                        pawnBaseline = (this.fields[x][y]  == PIECE_PAWN_WHITE ? 1 : 6);
                        int nextY = y + direction;
                        if (this.fieldEmpty(x, nextY)) {
                            res.add(new Move(x, y, x, nextY, this.fields[x][y]));
                            if (y == pawnBaseline && this.fieldEmpty(x, nextY + direction)) {
                                res.add(new Move(x, y, x, nextY + direction, this.fields[x][y]));
                            }
                        }
                        if (x > 0) {
                            if (isEnemyPiece(x - 1, nextY, player)) {
                                res.add(new Move(x, y, x - 1, nextY, this.fields[x][y], this.fields[x - 1][nextY]));
                            }
                            else if (this.enPassant == x - 1 && isEnPassantLine(y)) {
                                res.add(new Move(x, y, x - 1, nextY, this.fields[x][y], this.fields[x - 1][y], true));
                            }
                        }
                        if (x < 7) {
                            if (isEnemyPiece(x + 1, nextY, player)) {
                                res.add(new Move(x, y, x + 1, nextY, this.fields[x][y], this.fields[x + 1][nextY]));
                            }
                            else if (this.enPassant == x + 1 && isEnPassantLine(y)) {
                                res.add(new Move(x, y, x + 1, nextY, this.fields[x][y], this.fields[x + 1][y], true));
                            }
                        }
                        break;

                    case PIECE_KNIGHT_WHITE:
                    case PIECE_KNIGHT_BLACK:
                        for (int primDir = -2; primDir <= 2; primDir += 4) {
                            for (int secDir = -1; secDir <= 1; secDir += 2) {
                                aimX = x + primDir;
                                aimY = y + secDir;
                                if (isField(aimX, aimY) && !this.isOwnPiece(aimX, aimY, player)) {
                                    res.add(new Move(x, y, aimX, aimY, this.fields[x][y], this.fields[aimX][aimY]));
                                }
                                aimX = x + secDir;
                                aimY = y + primDir;
                                if (isField(aimX, aimY) && !this.isOwnPiece(aimX, aimY, player)) {
                                    res.add(new Move(x, y, aimX, aimY, this.fields[x][y], this.fields[aimX][aimY]));
                                }
                            }
                        }
                        break;

                    case PIECE_BISHOP_WHITE:
                    case PIECE_BISHOP_BLACK:
                        for (int dir1 = -1; dir1 <= 1; dir1 += 2) {
                            for (int dir2 = -1; dir2 <= 1; dir2 += 2) {
                                distance = 0;
                                while (true) {
                                    distance ++;
                                    aimX = x + dir1 * distance;
                                    aimY = y + dir2 * distance;
                                    if (!isField(aimX, aimY) || this.isOwnPiece(aimX, aimY, player)) {
                                        break;
                                    }
                                    if (this.isEnemyPiece(aimX, aimY, player)) {
                                        res.add(new Move(x, y, aimX, aimY, this.fields[x][y], this.fields[aimX][aimY]));
                                        break;
                                    }
                                    res.add(new Move(x, y, aimX, aimY, this.fields[x][y]));
                                }
                            }
                        }
                        break;

                    case PIECE_ROOK_WHITE:
                    case PIECE_ROOK_BLACK:
                        for (int angle = 0; angle <= 1; angle ++) {
                            for (int dir = -1; dir <= 1; dir++) {
                                distance = 0;
                                while (true) {
                                    distance ++;
                                    if (angle == 0) {
                                        aimX = x + dir * distance;
                                        aimY = y;
                                    }
                                    else {
                                        aimX = x;
                                        aimY = y + dir * distance;
                                    }
                                    if (!isField(aimX, aimY) || this.isOwnPiece(aimX, aimY, player)) {
                                        break;
                                    }
                                    if (this.isEnemyPiece(aimX, aimY, player)) {
                                        res.add(new Move(x, y, aimX, aimY, this.fields[x][y], this.fields[aimX][aimY]));
                                        break;
                                    }
                                    res.add(new Move(x, y, aimX, aimY, this.fields[x][y]));
                                }
                            }
                        }
                        break;

                    case PIECE_QUEEN_WHITE:
                    case PIECE_QUEEN_BLACK:
                        for (int dir1 = -1; dir1 <= 1; dir1++) {
                            for (int dir2 = -1; dir2 <= 1; dir2++) {

                                if (dir1 == 0 && dir2 == 0) {
                                    continue;
                                }

                                distance = 0;
                                while (true) {
                                    distance ++;
                                    aimX = x + dir1 * distance;
                                    aimY = y + dir2 * distance;
                                    if (!isField(aimX, aimY) || this.isOwnPiece(aimX, aimY, player)) {
                                        break;
                                    }
                                    if (this.isEnemyPiece(aimX, aimY, player)) {
                                        res.add(new Move(x, y, aimX, aimY, this.fields[x][y], this.fields[aimX][aimY]));
                                        break;
                                    }
                                    res.add(new Move(x, y, aimX, aimY, this.fields[x][y]));
                                }
                            }
                        }
                        break;

                    case PIECE_KING_WHITE:
                    case PIECE_KING_BLACK:
                        for (int dir1 = -1; dir1 <= 1; dir1++) {
                            for (int dir2 = -1; dir2 <= 1; dir2++) {

                                if (dir1 == 0 && dir2 == 0) {
                                    continue;
                                }

                                aimX = x + dir1;
                                aimY = y + dir2;
                                if (!isField(aimX, aimY) || this.isOwnPiece(aimX, aimY, player)) {
                                    continue;
                                }
                                res.add(new Move(x, y, aimX, aimY, this.fields[x][y], this.fields[aimX][aimY]));
                            }
                        }
                        if (castlingAvailableKingside(player) && fieldEmpty(5, kingY[player]) && fieldEmpty(6, kingY[player])) res.add(new Move(4, kingY[player], 6, kingY[player], this.fields[4][kingY[player]], true));
                        if (castlingAvailableQueenside(player) && fieldEmpty(3, kingY[player]) && fieldEmpty(2, kingY[player]) && fieldEmpty(1, kingY[player])) res.add(new Move(4, kingY[player], 2, kingY[player], this.fields[4][kingY[player]], true));
                        break;
                }
            }
        }

        return res;
    }

    public void forgetMoves() {

        this.moves.clear();
        if (movesAllSimple != null) this.movesAllSimple.clear();
        this.gotMovesSimple = this.gotMovesComplete = this.gotAllMovesSimple = false;
    }

    // Apply move without checking if it is valid
    public void moveUnchecked(Move move) {

        // Destination field will be occupied by moving piece after move, and the piece may undergo a conversion
        this.fields[move.toX][move.toY] = this.fields[move.fromX][move.fromY];
        if (move.pawnConversion != PIECE_NONE) {
            this.fields[move.toX][move.toY] = move.pawnConversion;
        }

        // Field where the piece moved from will be empty after move
        this.fields[move.fromX][move.fromY] = 0;

        // In case of an en passant capture, a field that is neither the start nor the destination will be empty
        if (move.enPassant) {
            this.fields[move.toX][move.fromY] = 0;
        }

        // If moving piece is king: update shortcut king coordinates and possibly perform castling
        if (move.piece == PIECE_KING_WHITE || move.piece == PIECE_KING_BLACK) {
            this.kingX[this.turn] = move.toX;
            this.kingY[this.turn] = move.toY;
            if (move.isCastling) {
                this.hasCastled[this.turn] = true;
                if (move.toX == 6) {
                    this.fields[5][this.kingY[this.turn]] = this.fields[7][this.kingY[this.turn]];
                    this.fields[7][this.kingY[this.turn]] = 0;
                }
                else {
                    this.fields[3][this.kingY[this.turn]] = this.fields[0][this.kingY[this.turn]];
                    this.fields[0][this.kingY[this.turn]] = 0;
                }
            }
            this.canCastle[this.turn] = 0;
        }
        else if (this.castlingAvailableKingside(this.turn) && move.fromX == 7 && (move.piece == PIECE_ROOK_WHITE || move.piece == PIECE_ROOK_BLACK)) this.disableCastlingKingside(this.turn);
        else if (this.castlingAvailableQueenside(this.turn) && move.fromX == 0 && (move.piece == PIECE_ROOK_WHITE || move.piece == PIECE_ROOK_BLACK)) this.disableCastlingQueenside(this.turn);

        if (this.castlingAvailableKingside(this.enemy()) && move.toX == 7 && (move.capturedPiece == PIECE_ROOK_WHITE || move.capturedPiece == PIECE_ROOK_BLACK)) this.disableCastlingKingside(this.enemy());
        else if (this.castlingAvailableQueenside(this.enemy()) && move.toX == 0 && (move.capturedPiece == PIECE_ROOK_WHITE || move.capturedPiece == PIECE_ROOK_BLACK)) this.disableCastlingQueenside(this.enemy());

        // Keep track of en passant status
        if ((move.piece == PIECE_PAWN_WHITE || move.piece == PIECE_PAWN_BLACK) && Math.abs(move.toY - move.fromY) == 2 && ((move.toX > 0 && isEnemyPiece(move.toX - 1, move.toY) || (move.toX < 7 && isEnemyPiece(move.toX + 1, move.toY))))) this.enPassant = move.toX;
        else this.enPassant = -1;

        // Keep track of 50-moves-rule status
        if (move.capturedPiece != 0 || move.piece == PIECE_PAWN_WHITE || move.piece == PIECE_PAWN_BLACK) this.fiftyMovesHalfmoves = 0;
        else this.fiftyMovesHalfmoves++;

        this.halfmoves++;

        this.gotMovesSimple = this.gotMovesComplete = this.gotAllMovesSimple = false;
        this.result = RESULT_UNKNOWN;
        this.turn = this.enemy();
    }

    // Apply move if it is valid
    public boolean move(Move move) {

        // Check if given move matches one of the self-calculated ones
        for (Move mv : this.getMoves()) {
            if (mv.fromX == move.fromX && mv.fromY == move.fromY && mv.toX == move.toX && mv.toY == move.toY && mv.pawnConversion == move.pawnConversion) {
                this.moveUnchecked(mv);
                return true;
            }
        }

        return false;
    }

    // Do some deletions from and additions to the calculated moves that are not always necessary and require considerable computation efforts
    public void refineMoves() {

        this.gotMovesComplete = true;

        List<Move> toDelete = new ArrayList<>();
        List<Move> toAdd = new ArrayList<>();

        OWNMOVES: for (Move mv : this.getMoves(false)) {
            mv.resultingBoard = new Board(this);
            mv.resultingBoard.moveUnchecked(mv);

            for (Move mvNext : mv.resultingBoard.getMoves(false)) {
                // If move leads to being in check: Remove it
                if (mvNext.capturedPiece == PIECE_KING_WHITE || mvNext.capturedPiece == PIECE_KING_BLACK) {
                    toDelete.add(mv);
                    continue OWNMOVES;
                }
                // If move is castling and castling line is attacked by enemy piece: Remove it
                if (mv.isCastling && mvNext.toY == this.kingY[this.turn] && (mvNext.toX == 4 || mvNext.toX == (mv.fromX + mv.toX) / 2)) {
                    toDelete.add(mv);
                    continue OWNMOVES;
                }
            }

            // Add pawn conversion variations
            if ((mv.piece == PIECE_PAWN_WHITE && mv.toY == 7) || (mv.piece == PIECE_PAWN_BLACK && mv.toY == 0)) {
                toDelete.add(mv);
                toAdd.add(new Move(mv.fromX, mv.fromY, mv.toX, mv.toY, mv.piece, mv.capturedPiece, (this.turn == PLAYER_WHITE ? PIECE_QUEEN_WHITE : PIECE_QUEEN_BLACK)));
                toAdd.add(new Move(mv.fromX, mv.fromY, mv.toX, mv.toY, mv.piece, mv.capturedPiece, (this.turn == PLAYER_WHITE ? PIECE_ROOK_WHITE : PIECE_ROOK_BLACK)));
                toAdd.add(new Move(mv.fromX, mv.fromY, mv.toX, mv.toY, mv.piece, mv.capturedPiece, (this.turn == PLAYER_WHITE ? PIECE_BISHOP_WHITE : PIECE_BISHOP_BLACK)));
                toAdd.add(new Move(mv.fromX, mv.fromY, mv.toX, mv.toY, mv.piece, mv.capturedPiece, (this.turn == PLAYER_WHITE ? PIECE_KNIGHT_WHITE : PIECE_KNIGHT_BLACK)));
            }
        }

        for (Move mv : toDelete) {
            this.moves.remove(mv);
        }

        for (Move mv : toAdd) {
            assert(mv.pawnConversion != PIECE_NONE) : "pawnConversion field not set in pawn conversion move";
            mv.resultingBoard = new Board(this);
            mv.resultingBoard.moveUnchecked(mv);
            this.moves.add(mv);
        }

    }

    public boolean isOwnPiece(int x, int y, int player) {

        return ((player == PLAYER_WHITE && this.fields[x][y] > 0) || (player == PLAYER_BLACK && this.fields[x][y] < 0));
    }

    public boolean isOwnPiece(int x, int y) {

        return this.isOwnPiece(x, y, this.turn);
    }

    public boolean isEnemyPiece(int x, int y, int player) {

        return ((player == PLAYER_BLACK && this.fields[x][y] > 0) || (player == PLAYER_WHITE && this.fields[x][y] < 0));
    }

    public boolean isEnemyPiece(int x, int y) {

        return this.isEnemyPiece(x, y, this.turn);
    }

    public boolean fieldEmpty(int x, int y) {

        return this.fields[x][y] == PIECE_NONE;
    }

    public boolean isWhitePiece(int x, int y) {

        return this.fields[x][y] > 0;
    }

    public boolean isBlackPiece(int x, int y) {

        return this.fields[x][y] < 0;
    }

    public boolean isPieceOfPlayer(int x, int y, int player) {

        return (player == PLAYER_WHITE && this.fields[x][y] > 0) || (player == PLAYER_BLACK && this.fields[x][y] < 0);
    }

    public static boolean isEnPassantLine(int y, int player) {

        return ((player == PLAYER_WHITE && y ==4) || (player == PLAYER_BLACK && y ==3));
    }

    public boolean isEnPassantLine(int y) {

        return this.isEnPassantLine(y, this.turn);
    }

    public int enemy() {

        return (this.turn == PLAYER_WHITE ? PLAYER_BLACK : PLAYER_WHITE);
    }

    public static boolean isField(int x, int y) {

        return (x >= 0 && x < 8 && y >= 0 && y < 8);
    }

    public void enableCastlingKingside(int player) {

        this.canCastle[player] |= (1<<0);
    }

    public static int enableCastlingKingsideGeneric(int canCastle) {

        return (canCastle |= (1<<0));
    }

    public void enableCastlingQueenside(int player) {

        this.canCastle[player] |= (1<<1);
    }

    public static int enableCastlingQueensideGeneric(int canCastle) {

        return (canCastle |= (1<<1));
    }

    public void disableCastlingKingside(int player) {
        this.canCastle[player] &= ~(1<<0);
    }

    public static int disableCastlingKingsideGeneric(int canCastle) {

        return (canCastle &= ~(1<<0));
    }

    public void disableCastlingQueenside(int player) {

        this.canCastle[player] &= ~(1<<1);
    }

    public static int disableCastlingQueensideGeneric(int canCastle) {

        return (canCastle &= ~(1<<1));
    }

    public boolean castlingAvailableKingside(int player) {

        return ((this.canCastle[player]>>0) % 2) > 0;
    }

    public static boolean castlingAvailableKingsideGeneric(int canCastle) {

        return ((canCastle>>0) % 2) > 0;
    }

    public boolean castlingAvailableQueenside(int player) {

        return ((this.canCastle[player]>>1) % 2) > 0;
    }

    public static boolean castlingAvailableQueensideGeneric(int canCastle) {

        return ((canCastle>>1) % 2) > 0;
    }

    // Parse game state in FEN format and set the pieces and game attributes accordingly
    public void setFen(String fen) {

        String[] fenComponents = fen.split(" ");
        if (fenComponents.length != 6) throw new InvalidParameterException("FEN should contain 6 fields, but contains " + fenComponents.length);
        if (!fenComponents[0].matches("([pPnNbBkKrRqQkK12345678]+/){7}[pPnNbBkKrRqQkK12345678]+")) throw new InvalidParameterException("Invalid FEN piece placement (first FEN field): " + fenComponents[0]);
        if (!fenComponents[1].matches("[wb]")) throw new InvalidParameterException("Second FEN field should indicate the color as 'w' or 'b', but is:" + fenComponents[1]);
        if (!fenComponents[2].matches("(KQ|K|Q|kq|k|q|KQkq|KQk|KQq|Kkq|Qkq|Kk|Kq|Qk|Qq|-)")) throw new InvalidParameterException("Third FEN field should indicate castling availability, but is: " + fenComponents[2]);
        if (!fenComponents[3].matches("(([a-h][36])|-)")) throw new InvalidParameterException("Fourth FEN field should indicate an en passant target, but is: " + fenComponents[3]);
        if (!fenComponents[4].matches("\\d+")) throw new InvalidParameterException("Fifth FEN field should indicate the (numeric) halfmove clock, but is: " + fenComponents[4]);
        if (!fenComponents[5].matches("\\d+")) throw new InvalidParameterException("Sixth FEN field should indicate the (numeric) fullmove number, but is: " + fenComponents[5]);

        int x = 0;
        int y = 7;
        int n;
        char c;

        for (int indexFen = 0; indexFen < fenComponents[0].length(); indexFen++) {

            c = fen.charAt(indexFen);

            if (c == '/') {
                if (x != 8) throw new InvalidParameterException("FEN row too short");
                y--;
                x = 0;
                continue;
            }

            if (x > 7) throw new InvalidParameterException("FEN row too long, now at index " + x + " and reading '" + c + "'");

            if ((n = Character.getNumericValue(c)) <= 8) {
                if (x  + n > 8) throw new InvalidParameterException("FEN row too long, empty fields do not fit");
                while (n > 0) {
                    this.fields[x][y] = 0;
                    x++;
                    n--;
                }
                continue;
            }

            this.fields[x][y] = pieceSymbolToNr(c);
            if (this.fields[x][y] == PIECE_KING_WHITE) {
                this.kingX[PLAYER_WHITE] = x;
                this.kingY[PLAYER_WHITE] = y;
            }
            else if (this.fields[x][y] == PIECE_KING_BLACK) {
                this.kingX[PLAYER_BLACK] = x;
                this.kingY[PLAYER_BLACK] = y;
            }
            x++;
        }

        this.turn = (fenComponents[1].charAt(0) == 'w' ? PLAYER_WHITE : PLAYER_BLACK);

        if (fenComponents[2].contains("K")) this.enableCastlingKingside(PLAYER_WHITE); else this.disableCastlingKingside(PLAYER_WHITE);
        if (fenComponents[2].contains("Q")) this.enableCastlingQueenside(PLAYER_WHITE); else this.disableCastlingQueenside(PLAYER_WHITE);
        if (fenComponents[2].contains("k")) this.enableCastlingKingside(PLAYER_BLACK); else this.disableCastlingKingside(PLAYER_BLACK);
        if (fenComponents[2].contains("q")) this.enableCastlingQueenside(PLAYER_BLACK); else this.disableCastlingQueenside(PLAYER_BLACK);

        this.enPassant = (fenComponents[3].equals("-") ? -1 : Character.getNumericValue(fenComponents[3].charAt(0)) - 10);

        this.fiftyMovesHalfmoves = Integer.parseInt(fenComponents[4]);

        this.halfmoves = Integer.parseInt(fenComponents[5]);
    }

    // FEN of current game
    public String getFen() {

        StringBuilder res = new StringBuilder();

        int n;

        for (int y = 7; y >= 0; y--) {
            n = 0;
            for (int x = 0; x < 8; x++) {
                if (this.fieldEmpty(x, y)) n++;
                else {
                    if (n > 0) {
                        res.append(n);
                        n = 0;
                    }
                    res.append(pieceNrToSymbol(this.fields[x][y]));
                }
            }
            if (n > 0) res.append(n);
            if (y != 0) res.append("/");
        }

        res.append(" ");

        res.append((this.turn == PLAYER_WHITE) ? "w" : "b");

        res.append(" ");

        if (this.canCastle[0] == 0 && this.canCastle[1] == 0) res.append("-");
        else {
            if (this.castlingAvailableKingside(PLAYER_WHITE)) res.append("K");
            if (this.castlingAvailableQueenside(PLAYER_WHITE)) res.append("Q");
            if (this.castlingAvailableKingside(PLAYER_BLACK)) res.append("k");
            if (this.castlingAvailableQueenside(PLAYER_BLACK)) res.append("q");
        }

        res.append(" ");

        if (this.enPassant < 0) res.append("-");
        else res.append((char)(this.enPassant + 97) + ((this.turn == PLAYER_WHITE) ? "6" : "3"));

        res.append(" ");

        res.append(this.fiftyMovesHalfmoves);

        res.append(" ");

        res.append((this.halfmoves / 2) + 1);

        return res.toString();
    }

    public static int pieceSymbolToNr(char c) {

        switch (c) {
            case 'P':
                return PIECE_PAWN_WHITE;
            case 'N':
                return PIECE_KNIGHT_WHITE;
            case 'B':
                return PIECE_BISHOP_WHITE;
            case 'R':
                return PIECE_ROOK_WHITE;
            case 'Q':
                return PIECE_QUEEN_WHITE;
            case 'K':
                return PIECE_KING_WHITE;
            case 'p':
                return PIECE_PAWN_BLACK;
            case 'n':
                return PIECE_KNIGHT_BLACK;
            case 'b':
                return PIECE_BISHOP_BLACK;
            case 'r':
                return PIECE_ROOK_BLACK;
            case 'q':
                return PIECE_QUEEN_BLACK;
            case 'k':
                return PIECE_KING_BLACK;
        }
        throw new InvalidParameterException("Invalid piece character");
    }

    public static char pieceNrToSymbol(int nr) {

        switch (nr) {
            case PIECE_PAWN_WHITE:
                return 'P';
            case PIECE_KNIGHT_WHITE:
                return 'N';
            case PIECE_BISHOP_WHITE:
                return 'B';
            case PIECE_ROOK_WHITE:
                return 'R';
            case PIECE_QUEEN_WHITE:
                return 'Q';
            case PIECE_KING_WHITE:
                return 'K';
            case PIECE_PAWN_BLACK:
                return 'p';
            case PIECE_KNIGHT_BLACK:
                return 'n';
            case PIECE_BISHOP_BLACK:
                return 'b';
            case PIECE_ROOK_BLACK:
                return 'r';
            case PIECE_QUEEN_BLACK:
                return 'q';
            case PIECE_KING_BLACK:
                return 'k';
        }
        throw new InvalidParameterException("Invalid piece number");
    }

    public static void main(String[] args) {

        Board board = new Board();
        board.setFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/R2QK2R w KQkq - 0 1");
        System.out.println(board.getMoves().toString());
    }
}
