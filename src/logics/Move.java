package logics;

import java.security.InvalidParameterException;

// Logic representation of a move of a piece, mostly without functionality beyond being a data structure
public class Move {

    public int fromX;
    public int fromY;
    public int toX;
    public int toY;

    public int piece;
    public int capturedPiece;

    public boolean isCastling;
    public boolean enPassant;

    public int pawnConversion;

    public Board resultingBoard;

    public int gain;
    public int estimationLongterm;

    // This constructor is mainly meant for being used in the API, as it fills captures and conversions automatically
    public Move(String move, String fen) {

        if (!move.matches("([a-h][1-8]x?[a-h][1-8][nbrqNBRQ]?)|(0-0(-0)?)")) throw new InvalidParameterException("Invalid move format");

        Board board = new Board(fen);

        if (move.matches("0-0(-0)?")) {
            this.isCastling = true;
            this.fromX = 4;
            this.fromY = this.toY = (board.turn == Board.PLAYER_WHITE ? 0 : 7);
            this.toX = (move.equals("0-0") ? 6 : 2);
            return;
        }

        boolean isCapture = (move.contains("x") ? true : false);

        try {
            this.pawnConversion = Board.pieceSymbolToNr(move.charAt(move.length() - 1));
        }
        catch (Exception e) {
            this.pawnConversion = Board.PIECE_NONE;
        }

        move = move.replace("x", "");

        this.fromX = Character.getNumericValue(move.charAt(0)) - 10;
        this.fromY = Character.getNumericValue(move.charAt(1)) - 1;
        this.toX = Character.getNumericValue(move.charAt(2)) - 10;
        this.toY = Character.getNumericValue(move.charAt(3)) - 1;

        this.capturedPiece = (isCapture ? board.fields[this.toX][this.toY] : Board.PIECE_NONE);
    }

    public Move(int fromX, int fromY, int toX, int toY, int piece, int capturedPiece, int pawnConversion) {

        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.piece = piece;
        this.capturedPiece = capturedPiece;
        this.pawnConversion = pawnConversion;
    }

    public Move(int fromX, int fromY, int toX, int toY, int piece, int capturedPiece) {

        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.piece = piece;
        this.capturedPiece = capturedPiece;
    }

    public Move(int fromX, int fromY, int toX, int toY, int piece, int capturedPiece, boolean enPassant) {

        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.piece = piece;
        this.capturedPiece = capturedPiece;
        this.enPassant = enPassant;
    }

    public Move(int fromX, int fromY, int toX, int toY, int piece) {

        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.piece = piece;
    }

    public Move(int fromX, int fromY, int toX, int toY) {

        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
    }

    public Move(int fromX, int fromY, int toX, int toY, int piece, boolean isCastling) {

        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.piece = piece;
        this.isCastling = isCastling;
    }

    public boolean isWhite() {

        return this.piece > 0;
    }

    @Override
    public String toString() {

        if (!this.isCastling) return ( "" + (char)(this.fromX + 97) + (this.fromY + 1) + (this.capturedPiece != 0 ? "x" : "") + (char)(this.toX + 97) + (this.toY + 1)) + (this.pawnConversion != Board.PIECE_NONE ? Board.pieceNrToSymbol(this.pawnConversion) : "");
        return ((this.toX > this.fromX) ? "0-0" : "0-0-0");
    }
}
