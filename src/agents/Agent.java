package agents;

import logics.Board;
import logics.Move;

// Allows for different agents (e.g. human, computer with various strategies, ...) to be used in a Game or in the api
public abstract class Agent {

    public Board board;
    public int playerNr;

    public void init(Board board, int playerNr) {

        this.board = board;
        this.playerNr = playerNr;
    }

    // Choose a move to do next
    public abstract Move chooseMove();
}
