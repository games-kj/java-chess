package agents;

import logics.Board;
import logics.Move;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

// Agent that reads the next move from a CLI user
public class AgentHuman extends Agent {

    @Override
    public Move chooseMove() {

        System.out.println("Please enter move, player" + this.playerNr + ":");
        List<Move> possibleMoves = this.board.getMoves();
        String readMove;

        // Keep reading moves until a valid one is put in
        // TODO: Consider pawn conversions
        while (true) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                 readMove = reader.readLine();
            }
            catch (IOException e) {
                System.out.println("IO-Error, please try again");
                continue;
            }
            // Allow different formats (ignore piece symbol and characters between coordinates)
            String moveAdjusted = readMove.replaceAll("([PNBRQK])([a-h][1-8])([x\\-\\s])([a-h][1-8])", "$2$4");

            // Regular move
            if (moveAdjusted.matches("([a-h][1-8]){2}")) {
                return new Move(Character.getNumericValue(moveAdjusted.charAt(0)) - 10, Character.getNumericValue(moveAdjusted.charAt(1)) - 1, Character.getNumericValue(moveAdjusted.charAt(2)) - 10, Character.getNumericValue(moveAdjusted.charAt(3)) - 1);
            }
            // Short castling
            else if (moveAdjusted.equals("0-0") || moveAdjusted.equals("O-O")) {
                if (this.playerNr == 0) {
                    return new Move(4, 0, 6, 0);
                }
                else {
                    return new Move (4, 7, 6, 7);
                }
            }
            // Long castling
            else if (moveAdjusted.equals("0-0-0") || moveAdjusted.equals("O-O-O")) {
                if (this.playerNr == 0) {
                    return new Move(4, 0, 2, 0);
                }
                else {
                    return new Move(4, 7, 2, 7);
                }
            }

            System.out.println("Invalid move. Expecting <startcoords><endcoords>, e.g. a2a3, or 0-0 resp. 0-0-0 for short resp. long castling. Please try again:");
        }

    }
}
