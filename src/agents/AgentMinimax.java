package agents;

import logics.Board;
import logics.Move;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

// Agent implementing the MinMax algorithm with some tweaks
public class AgentMinimax extends Agent {

    public int maxDepthRegular = 2;
    public int maxDepthInteresting = 4;
    public int maxDepthRegularInitial = 2;
    public int maxDepthInterestingInitial = 4;
    public long allowedMillis = 10000;
    public boolean timeUp = false;
    Instant timeStart;
    public HashMap<String, Integer> transpositionTable;

    public boolean updateEachMove = true;
    public boolean acceptFirstWinningMove = true;
    public boolean sortMovesAlways = true;
    public Move chosenMove;

    public final int BOUND_WIN = 9999999;
    static final int SIGNAL_WIN = 99999999;
    public final int SIGNAL_IMPOSSIBLE = 999999999;

    public static final int WORTH_PAWN_WHITE = 1;
    public static final int WORTH_KNIGHT_WHITE = 3;
    public static final int WORTH_BISHOP_WHITE = 3;
    public static final int WORTH_ROOK_WHITE = 5;
    public static final int WORTH_QUEEN_WHITE = 9;
    public static final int WORTH_KING_WHITE = 100;
    public static final int WORTH_PAWN_BLACK = -1;
    public static final int WORTH_KNIGHT_BLACK = -3;
    public static final int WORTH_BISHOP_BLACK = -3;
    public static final int WORTH_ROOK_BLACK = -5;
    public static final int WORTH_QUEEN_BLACK = -9;
    public static final int WORTH_KING_BLACK = -100;

    @Override
    public Move chooseMove() {

        this.transpositionTable = new HashMap<>();

        this.maxDepthRegular = this.maxDepthRegularInitial;
        this.maxDepthInteresting = this.maxDepthInterestingInitial;
        timeUp = false;

        this.chosenMove = this.board.getMoves().get(0);
        this.chosenMove.estimationLongterm = (this.playerNr == Board.PLAYER_WHITE ? -SIGNAL_IMPOSSIBLE : SIGNAL_IMPOSSIBLE);

        this.timeStart = Instant.now();

        int estimation;
        int nrCalculationRounds = 1;

        System.out.println("----- AgentMinmax starting calculation -----");
        System.out.println("Current board estimation: " + this.estimateBoard(this.board));

        // If only one move is possible: Simply "choose" that one
        if (board.getMoves().size() == 1) {
            return this.chosenMove;
        }

        // While time is not up and no win or loss has been found: Perform MinMax with increasing depths (starting over in each iteration)
        while (true) {

            System.out.println("Starting MinMax Calculation round " + nrCalculationRounds + " with maxDepthRegular=" + this.maxDepthRegular  + " and maxDepthInteresting=" + this.maxDepthInteresting);

            // Invoke MinMax, starting according to player color
            if (this.playerNr == 0) {
                estimation = this.maximize(board, 0, BOUND_WIN);
            }
            else {
                estimation = this.minimize(board, 0, -BOUND_WIN);
            }

            // No more time for further iterations
            if (timeUp) break;

            // Found a winning strategy => Don't need to search further
            if ((this.playerNr == Board.PLAYER_WHITE && estimation >= BOUND_WIN) || (this.playerNr == Board.PLAYER_BLACK && estimation <= -BOUND_WIN)) {
                System.out.println("Quitting after this round since win is already sure");
                break;
            }
            // Found out that there is a winning strategy for the enemy => Just go with best found move
            if ((this.playerNr == Board.PLAYER_BLACK && estimation >= BOUND_WIN) || (this.playerNr == Board.PLAYER_WHITE && estimation <= -BOUND_WIN)) {
                System.out.println("Oh no, I will loose, but I'm trying to survive as long as possible");
                break;
            }

            // Sort moves according to estimation of most recent MinMax iteration, which might help with pruning
            if (this.playerNr == Board.PLAYER_WHITE) this.board.moves.sort((a, b) -> b.estimationLongterm - a.estimationLongterm);
            else this.board.moves.sort((a, b) -> a.estimationLongterm - b.estimationLongterm);

            // Clear transposition table for next iteration
            this.transpositionTable.clear();

            // Dig deeper on next iteration
            this.maxDepthRegular += 1;
            this.maxDepthInteresting += 2;
            nrCalculationRounds ++;
        }

        return this.chosenMove;
    }

    // TODO: Maybe combine maximize and minimize functions to avoid duplicate code

    // Max-part of MinMax
    public int maximize(Board board, int depth, int foundMin) {

        int res = -SIGNAL_IMPOSSIBLE;
        Move bestMove = null;
        List<Move> moves = board.getMoves();

        // Sort moves according to direct gain, which might help in pruning
        if (this.sortMovesAlways && depth != 0) moves.sort((a, b) -> a.capturedPiece - b.capturedPiece);

        String transRep;

        // Go through available next moves
        for (Move mv : moves) {

            transRep = transpositionRepresentation(mv.resultingBoard);

            // If move is present in transposition table: Use already calculated value
            if (this.transpositionTable.get(transRep) != null) {
                mv.estimationLongterm = this.transpositionTable.get(transRep);
            }
            else {
                // If maximum depth is reached or game is decided: Use heuristic function
                if ((mv.capturedPiece == Board.PIECE_NONE && depth >= this.maxDepthRegular) || depth >= this.maxDepthInteresting || mv.resultingBoard.getResult() != Board.RESULT_OPEN) {
                    mv.estimationLongterm = this.estimateBoard(mv.resultingBoard);
                }
                else {
                    // Go to next depth with counterpart function
                    mv.estimationLongterm = this.minimize(mv.resultingBoard, depth + 1, res);
                }
                // Put move into transposition table
                this.transpositionTable.put(transRep, mv.estimationLongterm);
            }

            // If move looks better than previous favorite: Make it new favorite
            if (mv.estimationLongterm > res) {
                res = mv.estimationLongterm;
                bestMove = mv;
                // If we are at depth 0, i.e. moves are actual possible moves, and the chosen move should be updated immediately: Do it
                if (depth == 0 && this.updateEachMove && res > this.chosenMove.estimationLongterm) {
                    this.chosenMove = bestMove;
                    System.out.println("-- Updated chosenMove to " + this.chosenMove + ", expecting " + this.chosenMove.estimationLongterm);
                }
                //if (depth == 0 && !(this.updateEachMove && res > this.chosenMove.estimationLongterm)) {}
                // Alpha-Beta-pruning or found winning move
                if (res >= foundMin && (depth != 0 || this.acceptFirstWinningMove)) {
                    break;
                }
            }
            // If we are at depth 0 and the time is up: Wrap it up
            if (depth == 0 && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
                // If chosen move has never been set: Choose best found move
                if (this.chosenMove.estimationLongterm == -SIGNAL_IMPOSSIBLE) {
                    this.chosenMove = bestMove;
                }
                board.forgetMoves();
                this.timeUp = true;
                return res;
            }
        }

        // Went through all moves, time is not up

        // Set chosen move just in case it was not set earlier
        if (depth == 0) {
            this.chosenMove = bestMove;
            System.out.println("--- Finally updated chosenMove to " + this.chosenMove + ", expecting " + this.chosenMove.estimationLongterm);
        }
        else {
            board.forgetMoves();
        }

        // If a sure win resp. loss was found: Favor short resp. long remaining game
        if (res == SIGNAL_WIN) {
            res -= depth;
        }
        else if (res == -SIGNAL_WIN) {
            res += depth;
        }

        return res;
    }

    // Min-part of MinMax
    public int minimize(Board board, int depth, int foundMax) {

        int res = SIGNAL_IMPOSSIBLE;
        Move bestMove = null;
        List<Move> moves = board.getMoves();

        if (this.sortMovesAlways && depth != 0) moves.sort((a, b) -> b.capturedPiece - a.capturedPiece);

        String transRep;

        for (Move mv : moves) {

            transRep = transpositionRepresentation(mv.resultingBoard);

            if (this.transpositionTable.get(transRep) != null) {
                mv.estimationLongterm = this.transpositionTable.get(transRep);
            }
            else {
                if ((mv.capturedPiece == Board.PIECE_NONE && depth >= this.maxDepthRegular) || depth >= this.maxDepthInteresting || mv.resultingBoard.getResult() != Board.RESULT_OPEN) {
                    mv.estimationLongterm = this.estimateBoard(mv.resultingBoard);
                }
                else {
                    mv.estimationLongterm = this.maximize(mv.resultingBoard, depth + 1, res);
                }
                this.transpositionTable.put(transRep, mv.estimationLongterm);
            }

            if (mv.estimationLongterm < res) {
                res = mv.estimationLongterm;
                bestMove = mv;
                if (depth == 0 && this.updateEachMove && res < this.chosenMove.estimationLongterm) {
                    this.chosenMove = bestMove;
                    System.out.println("-- Updated chosenMove to " + this.chosenMove + ", expecting " + this.chosenMove.estimationLongterm);
                }
                if (res <= foundMax && (depth != 0 || this.acceptFirstWinningMove)) {
                    break;
                }
            }
            if (depth == 0 && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
                if (this.chosenMove.estimationLongterm == SIGNAL_IMPOSSIBLE) {
                    this.chosenMove = bestMove;
                }
                board.forgetMoves();
                this.timeUp = true;
                return res;
            }
        }

        if (depth == 0) {
            this.chosenMove = bestMove;
            System.out.println("--- Finally updated chosenMove to " + this.chosenMove + ", expecting " + this.chosenMove.estimationLongterm);
        }
        else {
            board.forgetMoves();
        }

        if (res == -SIGNAL_WIN) {
            res += depth;
        }
        else if (res == SIGNAL_WIN) {
            res -= depth;
        }

        return res;
    }

    // Heuristic value estimation
    public int estimateBoard(Board b) {

        b.gotEstimation = true;

        // Easy cases: Already finished games
        switch (b.getResult()) {
            case Board.RESULT_WIN_WHITE:
                return SIGNAL_WIN;
            case Board.RESULT_WIN_BLACK:
                return -SIGNAL_WIN;
            case Board.RESULT_TIE:
                return 0;

        }

        int res = 0;

        // Compare pure values of pieces of both players according to the commonly assigned values
        for (int[] column : b.fields) {
            for (int field: column) {

                switch (field) {
                    case Board.PIECE_NONE:
                    case Board.PIECE_KING_WHITE:
                    case Board.PIECE_KING_BLACK:
                        continue;
                    case Board.PIECE_PAWN_WHITE:
                        res += WORTH_PAWN_WHITE;
                        break;
                    case Board.PIECE_KNIGHT_WHITE:
                        res += WORTH_KNIGHT_WHITE;
                        break;
                    case Board.PIECE_BISHOP_WHITE:
                        res += WORTH_BISHOP_WHITE;
                        break;
                    case Board.PIECE_ROOK_WHITE:
                        res += WORTH_ROOK_WHITE;
                        break;
                    case Board.PIECE_QUEEN_WHITE:
                        res += WORTH_QUEEN_WHITE;
                        break;
                    case Board.PIECE_PAWN_BLACK:
                        res += WORTH_PAWN_BLACK;
                        break;
                    case Board.PIECE_KNIGHT_BLACK:
                        res += WORTH_KNIGHT_BLACK;
                        break;
                    case Board.PIECE_BISHOP_BLACK:
                        res += WORTH_BISHOP_BLACK;
                        break;
                    case Board.PIECE_ROOK_BLACK:
                        res += WORTH_ROOK_BLACK;
                        break;
                    case Board.PIECE_QUEEN_BLACK:
                        res += WORTH_QUEEN_BLACK;
                        break;
                }
            }
        }

        res *= 10000;

        // Bonus points for done or available castling moves
        if (b.hasCastled[Board.PLAYER_WHITE]) res += 4000;
        else {
            if (b.castlingAvailableKingside(Board.PLAYER_WHITE)) res += 1000;
            if (b.castlingAvailableQueenside(Board.PLAYER_WHITE)) res += 1000;
        }
        if (b.hasCastled[Board.PLAYER_BLACK]) res -= 4000;
        else {
            if (b.castlingAvailableKingside(Board.PLAYER_BLACK)) res -= 1000;
            if (b.castlingAvailableQueenside(Board.PLAYER_BLACK)) res -= 1000;
        }

        int numPiecesLeft = 0;

        // Penalize pieces for being farther away from the own or enemy king
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (b.isWhitePiece(x, y)) {
                    numPiecesLeft ++;
                    res -= Math.abs(x - b.kingX[Board.PLAYER_BLACK]) * 5;
                    res -= Math.abs(y - b.kingY[Board.PLAYER_BLACK]) * 5;
                    res -= Math.abs(x - b.kingX[Board.PLAYER_WHITE]) * 4;
                    res -= Math.abs(y - b.kingY[Board.PLAYER_WHITE]) * 4;
                }
                else if (b.isBlackPiece(x, y)) {
                    numPiecesLeft ++;
                    res += Math.abs(x - b.kingX[Board.PLAYER_WHITE]) * 5;
                    res += Math.abs(y - b.kingY[Board.PLAYER_WHITE]) * 5;
                    res += Math.abs(x - b.kingX[Board.PLAYER_BLACK]) * 4;
                    res += Math.abs(y - b.kingY[Board.PLAYER_BLACK]) * 4;
                }
            }
        }

        // Encourage pawns to move forward in late game
        if (numPiecesLeft < 20) {
            for (int[] column : b.fields)
                for (int y = 0; y < 8; y++) {
                    switch (column[y]) {
                        case Board.PIECE_PAWN_WHITE:
                            res += y * 30;
                            break;
                        case Board.PIECE_PAWN_BLACK:
                            res -= (7 - y) * 30;
                            break;
                    }
                }
        }

        List<Move> moves = b.getAllMovesSimple();

        for (Move move : moves) {
            if (move.isWhite()) {
                if (move.capturedPiece == Board.PIECE_NONE) res += 10;
                else res += 30;
            }
            else {
                if (move.capturedPiece == Board.PIECE_NONE) res -= 10;
                else res -= 30;
            }
        }

        // TODO : Include better heuristics for evaluating positions

        return res;
    }

    // Not human-friendly, but easily computable and complete representation of a board
    public static String transpositionRepresentation(Board b) {

        return Arrays.deepToString(b.fields) + Arrays.toString(b.canCastle) + b.turn;
    }
}
