package agents;

import logics.Board;
import logics.Move;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

// Agent implementing the MinMax algorithm with some tweaks
public class AgentMinimax2 extends Agent {

    public int maxDepthRegular = 4;
    public int maxDepthRegularA = 4;
    public int maxDepthRegularB = 4;
    public int maxDepthRegularC = 4;
    public int maxDepthInteresting = 6;
    public int maxDepthInterestingA = 6;
    public int maxDepthInterestingB = 6;
    public int maxDepthInterestingC = 6;
    public int maxTemporaryRegression = 9500;
    public int maxTemporaryRegressionA = 9500;
    public int maxTemporaryRegressionB = 9500;
    public int maxTemporaryRegressionC = 9500;
    public int maxMoves = 1;
    Instant timeStart;

    public int allowedMillis = 20000;
    boolean timeUp = false;
    public int round = 0;
    public HashMap<String, Integer>[] transpositionTables;

    public List<Integer> depths = new ArrayList<>();
    public int maxDepth = 0;
    public int numHitsTranspositionTable = 0;

    public boolean acceptFirstWinningMove = true;
    public boolean sortMovesAlways = true;
    public Move chosenMove;

    public final int BOUND_WIN = 9999999;
    static final int SIGNAL_WIN = 99999999;
    public final int SIGNAL_IMPOSSIBLE = 999999999;
    public final int ABSOLUTE_MAX_DEPTH = 64;

    public static final int WORTH_PAWN_WHITE = 1;
    public static final int WORTH_KNIGHT_WHITE = 3;
    public static final int WORTH_BISHOP_WHITE = 3;
    public static final int WORTH_ROOK_WHITE = 5;
    public static final int WORTH_QUEEN_WHITE = 9;
    public static final int WORTH_KING_WHITE = 100;
    public static final int WORTH_PAWN_BLACK = -1;
    public static final int WORTH_KNIGHT_BLACK = -3;
    public static final int WORTH_BISHOP_BLACK = -3;
    public static final int WORTH_ROOK_BLACK = -5;
    public static final int WORTH_QUEEN_BLACK = -9;
    public static final int WORTH_KING_BLACK = -100;

    @Override
    public Move chooseMove() {


        int estimationStart = this.estimateBoard(this.board);
        System.out.println("\n\n===== AgentMinmax2D starting calculation =====");
        System.out.println("Current board estimation: " + estimationStart);

        int numMoves = this.board.getMoves().size();

        depths.clear();
        maxDepth = 0;

        this.transpositionTables = new HashMap[ABSOLUTE_MAX_DEPTH];
        for (int i = 0; i < ABSOLUTE_MAX_DEPTH; i++) this.transpositionTables[i] = new HashMap<>();

        this.chosenMove = this.board.getMoves().get(0);
        this.chosenMove.estimationLongterm = (this.playerNr == Board.PLAYER_WHITE ? -SIGNAL_IMPOSSIBLE : SIGNAL_IMPOSSIBLE);

        this.timeUp = false;
        this.timeStart = Instant.now();

        int estimation;
        int nrCalculationRounds = 1;

        // If only one move is possible: Simply "choose" that one
        if (numMoves == 1) {
            return this.chosenMove;
        }

        // X

        // Invoke MinMax, starting according to player color

        for (Move mv: board.getMoves()) {
            mv.estimationLongterm = this.estimateBoard(mv.resultingBoard);
        }
        if (this.playerNr == Board.PLAYER_WHITE) this.board.moves.sort((a, b) -> b.estimationLongterm - a.estimationLongterm);
        else this.board.moves.sort((a, b) -> a.estimationLongterm - b.estimationLongterm);

        int numPieces = board.getNumPieces();
        if (numPieces > 28) {
            maxDepthRegularA = 2;
            maxDepthInterestingA = 4;
            maxTemporaryRegressionA = 100;

            maxDepthRegularB = 4;
            maxDepthInterestingB = 4;
            maxTemporaryRegressionB = 1000;

            maxDepthRegularC = 4;
            maxDepthInterestingC = 6;
            maxTemporaryRegressionC = 6000;
        }
        else if (numPieces > 20) {
            maxDepthRegularA = 2;
            maxDepthInterestingA = 4;
            maxTemporaryRegressionA = 1000;

            maxDepthRegularB = 4;
            maxDepthInterestingB = 4;
            maxTemporaryRegressionB = 6000;

            maxDepthRegularC = 4;
            maxDepthInterestingC = 6;
            maxTemporaryRegressionC = 19500;
        }
        else if (numPieces > 10) {
            maxDepthRegularA = 2;
            maxDepthInterestingA = 4;
            maxTemporaryRegressionA = 1000;

            maxDepthRegularB = 4;
            maxDepthInterestingB = 6;
            maxTemporaryRegressionB = 9500;

            maxDepthRegularC = 6;
            maxDepthInterestingC = 8;
            maxTemporaryRegressionC = 19500;
        }
        else {
            maxDepthRegularA = 4;
            maxDepthInterestingA = 6;
            maxTemporaryRegressionA = 1000;

            maxDepthRegularB = 6;
            maxDepthInterestingB = 8;
            maxTemporaryRegressionB = 9500;

            maxDepthRegularC = 8;
            maxDepthInterestingC = 10;
            maxTemporaryRegressionC = 19500;
        }

        this.round = 0;

        maxMoves = numMoves;
        System.out.println("Starting round 1 with " + maxMoves + " moves and " + numPieces + " pieces");
        maxDepthRegular = maxDepthRegularA;
        maxDepthInteresting = maxDepthInterestingA;
        maxTemporaryRegression = maxTemporaryRegressionA;
        numHitsTranspositionTable = 0;
        if (this.playerNr == 0) {
            estimation = this.maximize(board, 0, BOUND_WIN, BOUND_WIN, -BOUND_WIN);
        }
        else {
            estimation = this.minimize(board, 0, -BOUND_WIN, BOUND_WIN, -BOUND_WIN);
        }

        if (!timeUp) {
            for (int i = 0; i < ABSOLUTE_MAX_DEPTH; i++) this.transpositionTables[i].clear();
            if (this.playerNr == Board.PLAYER_WHITE)
                this.board.moves.sort((a, b) -> b.estimationLongterm - a.estimationLongterm);
            else this.board.moves.sort((a, b) -> a.estimationLongterm - b.estimationLongterm);
            for (Move mv : board.getMoves()) {
                mv.estimationLongterm = (this.playerNr == Board.PLAYER_WHITE) ? -SIGNAL_IMPOSSIBLE : SIGNAL_IMPOSSIBLE;
            }

            this.round = 1;

            maxMoves = 10;
            System.out.println("Transposition table hits: " + this.numHitsTranspositionTable);
            System.out.println("Starting round 2 with " + maxMoves + " moves");
            maxDepthRegular = maxDepthRegularB;
            maxDepthInteresting = maxDepthInterestingB;
            maxTemporaryRegression = maxTemporaryRegressionB;
            numHitsTranspositionTable = 0;
            if (this.playerNr == 0) {
                if (this.chosenMove.estimationLongterm > estimationStart)
                    estimation = this.maximize(board, 0, BOUND_WIN, BOUND_WIN, estimationStart);
                else if (this.chosenMove.estimationLongterm < estimationStart)
                    estimation = this.maximize(board, 0, BOUND_WIN, estimationStart, -BOUND_WIN);
                else estimation = this.maximize(board, 0, BOUND_WIN, BOUND_WIN, -BOUND_WIN);
            } else {
                if (this.chosenMove.estimationLongterm < estimationStart)
                    estimation = this.minimize(board, 0, -BOUND_WIN, estimationStart, -BOUND_WIN);
                else if (this.chosenMove.estimationLongterm > estimationStart)
                    estimation = this.minimize(board, 0, -BOUND_WIN, BOUND_WIN, estimationStart);
                else estimation = this.minimize(board, 0, -BOUND_WIN, BOUND_WIN, -BOUND_WIN);
            }
        }

        if (!timeUp) {
            for (int i = 0; i < ABSOLUTE_MAX_DEPTH; i++) this.transpositionTables[i].clear();
            if (this.playerNr == Board.PLAYER_WHITE)
                this.board.moves.sort((a, b) -> b.estimationLongterm - a.estimationLongterm);
            else this.board.moves.sort((a, b) -> a.estimationLongterm - b.estimationLongterm);

            this.round = 2;

            maxMoves = 3;
            System.out.println("Transposition table hits: " + this.numHitsTranspositionTable);
            System.out.println("Starting round 3 with " + maxMoves + " moves");
            maxDepthRegular = maxDepthRegularC;
            maxDepthInteresting = maxDepthInterestingC;
            maxTemporaryRegression = maxTemporaryRegressionC;
            numHitsTranspositionTable = 0;
            if (this.playerNr == 0) {
                if (this.chosenMove.estimationLongterm > estimationStart)
                    estimation = this.maximize(board, 0, BOUND_WIN, BOUND_WIN, estimationStart);
                else if (this.chosenMove.estimationLongterm < estimationStart)
                    estimation = this.maximize(board, 0, BOUND_WIN, estimationStart, -BOUND_WIN);
                else estimation = this.maximize(board, 0, BOUND_WIN, BOUND_WIN, -BOUND_WIN);
            } else {
                if (this.chosenMove.estimationLongterm < estimationStart)
                    estimation = this.minimize(board, 0, -BOUND_WIN, estimationStart, -BOUND_WIN);
                else if (this.chosenMove.estimationLongterm > estimationStart)
                    estimation = this.minimize(board, 0, -BOUND_WIN, BOUND_WIN, estimationStart);
                else estimation = this.minimize(board, 0, -BOUND_WIN, BOUND_WIN, -BOUND_WIN);
            }
        }

        // X

        System.out.println("Transposition table hits: " + this.numHitsTranspositionTable);
        System.out.println("Finished after " + Duration.between(this.timeStart, Instant.now()).toMillis() + " ms");
        return this.chosenMove;
    }

    // TODO: Maybe combine maximize and minimize functions to avoid duplicate code

    // Max-part of MinMax
    public int maximize(Board board, int depth, int foundMin, int bestMin, int bestMax) {

        if (depth >= 4 && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
            this.timeUp = true;
            return 0;
        }

        int res = -SIGNAL_IMPOSSIBLE;
        int estimation;
        Move bestMove = null;
        List<Move> moves = board.getMoves();
        boolean foundInTranspositionTable = false;

        // Sort moves according to direct gain, which might help in pruning
        if (this.sortMovesAlways && depth != 0) moves.sort((a, b) -> a.capturedPiece - b.capturedPiece);

        String transRep;

        int numCheckedMoves = 0;

        // Go through available next moves
        for (Move mv : moves) {

            foundInTranspositionTable = false;

            transRep = transpositionRepresentation(mv.resultingBoard);
            // If move is present in transposition table: Use already calculated value
            for (int i = 0; i <= depth; i++) {
                if (this.transpositionTables[i].get(transRep) != null) {
                    mv.estimationLongterm = this.transpositionTables[i].get(transRep);
                    foundInTranspositionTable = true;
                    this.numHitsTranspositionTable ++;
                    break;
                }
            }

            if (!foundInTranspositionTable) {
                estimation = this.estimateBoard(mv.resultingBoard);
                // If maximum depth is reached or game is decided: Use heuristic function
                if (estimation - bestMax < -maxTemporaryRegression || (depth % 2 == 0 && ((mv.capturedPiece == Board.PIECE_NONE && depth >= this.maxDepthRegular) || depth >= this.maxDepthInteresting)) || mv.resultingBoard.getResult() != Board.RESULT_OPEN) {
                    if (!depths.contains(depth)) depths.add(depth);
                    if (depth > maxDepth) maxDepth = depth;
                    mv.estimationLongterm = estimation;
                }
                else {
                    // Go to next depth with counterpart function
                    mv.estimationLongterm = this.minimize(mv.resultingBoard, depth + 1, res, bestMin, bestMax);
                }
                // Put move into transposition table
                this.transpositionTables[depth].put(transRep, mv.estimationLongterm);
            }

            // If move looks better than previous favorite: Make it new favorite
            if (mv.estimationLongterm > res) {
                res = mv.estimationLongterm;
                bestMove = mv;
                //if (depth == 0 && !(this.updateEachMove && res > this.chosenMove.estimationLongterm)) {}
                // Alpha-Beta-pruning or found winning move
                if (res >= foundMin && (depth != 0 || this.acceptFirstWinningMove)) {
                    break;
                }
            }

            if (mv.estimationLongterm > bestMax) {
                bestMax = mv.estimationLongterm;
            }

            if (depth == 0) {
                numCheckedMoves ++;
                if (numCheckedMoves >= maxMoves) break;
            }

            if (timeUp) return 0;
        }

        // Went through all moves, time is not up

        // Set chosen move just in case it was not set earlier
        if (depth == 0 && !timeUp) {
            this.chosenMove = bestMove;
            System.out.println("--- Finally updated chosenMove to " + this.chosenMove + ", expecting " + this.chosenMove.estimationLongterm);
        }
        else {
            board.forgetMoves();
        }

        // If a sure win resp. loss was found: Favor short resp. long remaining game
        if (res == SIGNAL_WIN) {
            res -= depth;
        }
        else if (res == -SIGNAL_WIN) {
            res += depth;
        }

        return res;
    }

    // Min-part of MinMax
    public int minimize(Board board, int depth, int foundMax, int bestMin, int bestMax) {

        int res = SIGNAL_IMPOSSIBLE;
        int estimation;
        Move bestMove = null;
        List<Move> moves = board.getMoves();
        boolean foundInTranspositionTable = false;

        if (this.sortMovesAlways && depth != 0) moves.sort((a, b) -> b.capturedPiece - a.capturedPiece);

        String transRep;

        int numCheckedMoves = 0;

        for (Move mv : moves) {

            foundInTranspositionTable = false;

            transRep = transpositionRepresentation(mv.resultingBoard);

            for (int i = 0; i <= depth; i++) {
                if (this.transpositionTables[i].get(transRep) != null) {
                    mv.estimationLongterm = this.transpositionTables[i].get(transRep);
                    foundInTranspositionTable = true;
                    this.numHitsTranspositionTable ++;
                    break;
                }
            }

            if (!foundInTranspositionTable) {
                estimation = this.estimateBoard(mv.resultingBoard);

                if (estimation - bestMin > maxTemporaryRegression || (depth % 2 == 0 && ((mv.capturedPiece == Board.PIECE_NONE && depth >= this.maxDepthRegular) || depth >= this.maxDepthInteresting)) || mv.resultingBoard.getResult() != Board.RESULT_OPEN) {
                    if (!depths.contains(depth)) depths.add(depth);
                    if (depth > maxDepth) maxDepth = depth;
                    mv.estimationLongterm = estimation;
                }
                else {
                    mv.estimationLongterm = this.maximize(mv.resultingBoard, depth + 1, res, bestMin, bestMax);
                }
                this.transpositionTables[depth].put(transRep, mv.estimationLongterm);
            }

            if (depth == 0) {
                System.out.println("-- Move " + mv + " expected to yield " + mv.estimationLongterm + " at depth " + maxDepth);
                maxDepth = 0;
            }

            if (mv.estimationLongterm < res) {
                res = mv.estimationLongterm;
                bestMove = mv;
                if (res <= foundMax && (depth != 0 || this.acceptFirstWinningMove)) {
                    break;
                }
            }

            if (mv.estimationLongterm < bestMin) {
                bestMin = mv.estimationLongterm;
            }

            if (depth == 0) {
                numCheckedMoves ++;
                if (numCheckedMoves >= maxMoves) break;
            }

            if (timeUp) return 0;
        }

        if (depth == 0 && !timeUp) {
            this.chosenMove = bestMove;
            System.out.println("--- Finally updated chosenMove to " + this.chosenMove + ", expecting " + this.chosenMove.estimationLongterm);
        }
        else {
            board.forgetMoves();
        }

        if (res == -SIGNAL_WIN) {
            res += depth;
        }
        else if (res == SIGNAL_WIN) {
            res -= depth;
        }

        return res;
    }

    // Heuristic value estimation
    public int estimateBoard(Board b) {

        b.gotEstimation = true;

        // Easy cases: Already finished games
        switch (b.getResult()) {
            case Board.RESULT_WIN_WHITE:
                return SIGNAL_WIN;
            case Board.RESULT_WIN_BLACK:
                return -SIGNAL_WIN;
            case Board.RESULT_TIE:
                return 0;

        }

        int res = 0;

        // Compare pure values of pieces of both players according to the commonly assigned values
        for (int[] column : b.fields) {
            for (int field: column) {

                switch (field) {
                    case Board.PIECE_NONE:
                    case Board.PIECE_KING_WHITE:
                    case Board.PIECE_KING_BLACK:
                        continue;
                    case Board.PIECE_PAWN_WHITE:
                        res += WORTH_PAWN_WHITE;
                        break;
                    case Board.PIECE_KNIGHT_WHITE:
                        res += WORTH_KNIGHT_WHITE;
                        break;
                    case Board.PIECE_BISHOP_WHITE:
                        res += WORTH_BISHOP_WHITE;
                        break;
                    case Board.PIECE_ROOK_WHITE:
                        res += WORTH_ROOK_WHITE;
                        break;
                    case Board.PIECE_QUEEN_WHITE:
                        res += WORTH_QUEEN_WHITE;
                        break;
                    case Board.PIECE_PAWN_BLACK:
                        res += WORTH_PAWN_BLACK;
                        break;
                    case Board.PIECE_KNIGHT_BLACK:
                        res += WORTH_KNIGHT_BLACK;
                        break;
                    case Board.PIECE_BISHOP_BLACK:
                        res += WORTH_BISHOP_BLACK;
                        break;
                    case Board.PIECE_ROOK_BLACK:
                        res += WORTH_ROOK_BLACK;
                        break;
                    case Board.PIECE_QUEEN_BLACK:
                        res += WORTH_QUEEN_BLACK;
                        break;
                }
            }
        }

        res *= 10000;

        // Bonus points for done or available castling moves
        if (b.hasCastled[Board.PLAYER_WHITE]) res += 4000;
        else {
            if (b.castlingAvailableKingside(Board.PLAYER_WHITE)) res += 1000;
            if (b.castlingAvailableQueenside(Board.PLAYER_WHITE)) res += 1000;
        }
        if (b.hasCastled[Board.PLAYER_BLACK]) res -= 4000;
        else {
            if (b.castlingAvailableKingside(Board.PLAYER_BLACK)) res -= 1000;
            if (b.castlingAvailableQueenside(Board.PLAYER_BLACK)) res -= 1000;
        }

        int numPiecesLeft = 0;

        // Penalize pieces for being farther away from the own or enemy king
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (b.isWhitePiece(x, y)) {
                    numPiecesLeft ++;
                    res -= Math.abs(x - b.kingX[Board.PLAYER_BLACK]) * 5;
                    res -= Math.abs(y - b.kingY[Board.PLAYER_BLACK]) * 5;
                    res -= Math.abs(x - b.kingX[Board.PLAYER_WHITE]) * 4;
                    res -= Math.abs(y - b.kingY[Board.PLAYER_WHITE]) * 4;
                }
                else if (b.isBlackPiece(x, y)) {
                    numPiecesLeft ++;
                    res += Math.abs(x - b.kingX[Board.PLAYER_WHITE]) * 5;
                    res += Math.abs(y - b.kingY[Board.PLAYER_WHITE]) * 5;
                    res += Math.abs(x - b.kingX[Board.PLAYER_BLACK]) * 4;
                    res += Math.abs(y - b.kingY[Board.PLAYER_BLACK]) * 4;
                }
            }
        }

        // Encourage pawns to move forward in late game
        if (numPiecesLeft < 20) {
            for (int[] column : b.fields)
                for (int y = 0; y < 8; y++) {
                    switch (column[y]) {
                        case Board.PIECE_PAWN_WHITE:
                            res += y * 30;
                            break;
                        case Board.PIECE_PAWN_BLACK:
                            res -= (7 - y) * 30;
                            break;
                    }
                }
        }

        List<Move> moves = b.getAllMovesSimple();

        for (Move move : moves) {
            if (move.isWhite()) {
                if (move.capturedPiece == Board.PIECE_NONE) res += 10;
                else res += 30;
            }
            else {
                if (move.capturedPiece == Board.PIECE_NONE) res -= 10;
                else res -= 30;
            }
        }

        // TODO : Include better heuristics for evaluating positions

        return res;
    }

    // Not human-friendly, but easily computable and complete representation of a board
    public static String transpositionRepresentation(Board b) {

        return Arrays.deepToString(b.fields) + Arrays.toString(b.canCastle) + b.turn;
    }
}
