package gui;

import logics.Board;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

/**
 *
 * @author Johannes
 */

// Graphical representation of a field, as a helper of the graphical board
public class GField extends JPanel{

    ImageIcon[] pieceIcons;
    JLabel[] pieceLabels = new JLabel[13];
    int piece;
    int x;
    int y;

    public GField(int x, int y, ImageIcon pieceIcons[]) {

        this.x = x;
        this.y = y;

        this.pieceIcons = pieceIcons;
        for (int i = 0; i < 13; i++) {
            this.pieceLabels[i] = new JLabel(pieceIcons[i]);
            this.pieceLabels[i].setVisible(true);
        }
        this.piece = 0;
        this.setVisible(true);
    }

    @Override
    public void paint(Graphics g) {

        super.paint(g);
        Graphics2D g2D = (Graphics2D) g;
        g2D.drawImage(this.pieceIcons[pieceToIconIndex(this.piece)].getImage(), 0, 0, null);
    }

    public void setPiece(int pieceNr) {

        if (pieceNr == this.piece) {
            return;
        }
        this.piece = pieceNr;
        //this.removeAll();
        //if (pieceNr == 0) {
        //    return;
        //}
        //this.add(this.pieceLabels[pieceNr]);
        this.repaint();
    }

    public static int pieceToIconIndex(int piece) {

        switch (piece) {
            case Board.PIECE_PAWN_WHITE:
                return 1;
            case Board.PIECE_KNIGHT_WHITE:
                return 2;
            case Board.PIECE_BISHOP_WHITE:
                return 3;
            case Board.PIECE_ROOK_WHITE:
                return 4;
            case Board.PIECE_QUEEN_WHITE:
                return 5;
            case Board.PIECE_KING_WHITE:
                return 6;
            case Board.PIECE_PAWN_BLACK:
                return 7;
            case Board.PIECE_KNIGHT_BLACK:
                return 8;
            case Board.PIECE_BISHOP_BLACK:
                return 9;
            case Board.PIECE_ROOK_BLACK:
                return 10;
            case Board.PIECE_QUEEN_BLACK:
                return 11;
            case Board.PIECE_KING_BLACK:
                return 12;
        }

        return 0;
    }

}

