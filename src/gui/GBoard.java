package gui;

import java.awt.Color;
import javax.swing.JFrame;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import javax.swing.ImageIcon;
import logics.Board;

/**
 *
 * @author Johannes
 */

// Graphical board representation
public class GBoard {

    public JFrame board;
    public int boardsize = 800;
    public int fieldsize = 100;
    public GField[][] fields = new GField[8][8];
    public ImageIcon[] pieceIcons = new ImageIcon[13];
    public final String fenInitial = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    public Board logicBoard;

    public GBoard(Board logicBoard) {

        this.logicBoard = logicBoard;

        board = new JFrame();

        // Piece icons
        BufferedImage piecesPic = null;
        try {
            piecesPic = ImageIO.read(new File(new File("").getAbsolutePath().concat("/src/gui/pieces.png")));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        this.pieceIcons[0] = new ImageIcon(piecesPic.getSubimage(0, 0, 2, 2).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[6] = new ImageIcon(piecesPic.getSubimage(0, 0, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[5] = new ImageIcon(piecesPic.getSubimage(200, 0, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[3] = new ImageIcon(piecesPic.getSubimage(400, 0, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[2] = new ImageIcon(piecesPic.getSubimage(600, 0, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[4] = new ImageIcon(piecesPic.getSubimage(800, 0, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[1] = new ImageIcon(piecesPic.getSubimage(1000, 0, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[12] = new ImageIcon(piecesPic.getSubimage(0, 200, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[11] = new ImageIcon(piecesPic.getSubimage(200, 200, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[9] = new ImageIcon(piecesPic.getSubimage(400, 200, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[8] = new ImageIcon(piecesPic.getSubimage(600, 200, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[10] = new ImageIcon(piecesPic.getSubimage(800, 200, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));
        this.pieceIcons[7] = new ImageIcon(piecesPic.getSubimage(1000, 200, 200, 200).getScaledInstance(fieldsize, fieldsize, BufferedImage.SCALE_SMOOTH));

        this.paintBoard();
        this.paintInitialPieces();
    }

    public void paintBoard() {

        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.setLayout(null);
        board.setSize(boardsize, boardsize);

        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                this.fields[x][y] = new GField(x, y, this.pieceIcons);
                this.fields[x][y].setBackground(((x + y) % 2 == 0) ? Color.gray : Color.white);
                this.fields[x][y].setBounds(x * this.fieldsize, this.boardsize - ((y + 1) * this.fieldsize), fieldsize, fieldsize);
                board.add(this.fields[x][y]);
            }
        }

        board.setVisible(true);
    }

    public void paintInitialPieces() {

        int x = 0;
        int y = 7;
        char c;
        int n;

        // Parse initial positions FEN and set pieces accordingly
        for (int i = 0; i < this.fenInitial.length(); i++) {
            c = this.fenInitial.charAt(i);
            if (c == ' ') {
                break;
            }
            if (c == '/') {
                y --;
                x = 0;
                continue;
            }
            n = Character.getNumericValue(c);
            if (n <= 8) {
                x += n;
                continue;
            }
            this.fields[x][y].setPiece(c);
            x++;
        }
    }

    public void update() {

        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                this.fields[x][y].setPiece(this.logicBoard.fields[x][y]);
            }
        }
    }

    public void movePiece(int fromX, int fromY, int toX, int toY) {

        this.fields[toX][toY].setPiece(this.fields[fromX][fromY].piece);
        this.fields[fromX][fromY].setPiece(0);
    }

    public void start() {
        this.paintBoard();
        this.paintInitialPieces();
    }

    public void close() {
        this.board.dispatchEvent(new WindowEvent(this.board, WindowEvent.WINDOW_CLOSING));
    }

}
