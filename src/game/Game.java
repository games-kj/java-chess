package game;


import java.io.IOException;

import gui.GBoard;
import agents.Agent;
import agents.AgentHuman;
import agents.AgentMinimax;
import agents.AgentMinimax2;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Represents a game of chess, letting two agents compete and managing a GUI
public class Game {

    public GBoard gui;
    public Board board;
    public Agent player1;
    public Agent player2;
    public int turn = 0;
    public int winner = -1;

    public Game(Agent player1, Agent player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void start() throws IOException {

        if (this.gui != null) {
            this.gui.close();
        }

        this.board = new Board();
        this.board.start();
        this.turn = this.board.turn;

        this.player1.init(board, 0);
        this.player2.init(board, 1);

        this.gui = new GBoard(this.board);
        this.gui.update();

        while(true) {
            System.out.println(this.board.getFen());
            this.doTurn();
            this.gui.update();
            if (this.board.getResult() != Board.RESULT_OPEN) {
                break;
            }
        }
        if (this.board.result == 2) {
            System.out.println("IT'S A TIE");
        }
        else {
            System.out.println("WINNER: player" + this.board.result);
        }
    }

    public void doTurn() {

        Move move;

        while(true) {
            move = (turn == 0 ? player1.chooseMove() : player2.chooseMove());
            if (this.board.move(move)) {
                break;
            }
            System.out.println("Board rejected the move (" + move.fromX + "/" + move.fromY + " to " + move.toX + "/" + move.toY + ")");
        }

        this.turn ^= 1;
    }

    // If you use this as the project's main function, you can set which players will compete here
    public static void main(String[] args) throws IOException {

        Agent player1 = new AgentHuman();
        Agent player2 = new AgentMinimax2();

        Game game = new Game(player1, player2);

        game.start();
    }

}
