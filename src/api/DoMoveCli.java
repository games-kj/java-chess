package api;

import logics.Board;
import logics.Move;

import java.util.stream.Collectors;

// Accepts a board and move as arguments, applies the move to the board, and prints the results
// TODO: Documentation of input and output format and parsing details
public class DoMoveCli {

    public static void main(String[] args) {

        Board board;
        Move move;

        try {
            board = new Board(args[0]);
            move = new Move(args[1], args[0]);
        }
        catch (Exception e) {
            System.out.println("error 1");
            return;
        }

        if (!board.move(move)) {
            System.out.println("error 2");
            return;
        }

        String endresult;

        switch (board.getResult()) {
            case Board.RESULT_OPEN:
                endresult = "o";
                break;
            case Board.RESULT_WIN_WHITE:
                endresult = "w";
                break;
            case Board.RESULT_WIN_BLACK:
                endresult = "b";
                break;
            case Board.RESULT_TIE:
                endresult = "t";
                break;
            default:
                System.out.println("error 3");
                return;
        }

        System.out.println("fen: " + board.getFen());
        System.out.println("moves: " + board.getMoves().stream().map(mv -> "\"" + mv.toString() + "\"").collect(Collectors.toList()).toString());
        System.out.println("endresult: " + endresult);
    }
}
