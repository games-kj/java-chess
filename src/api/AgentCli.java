package api;

import agents.AgentMinimax;
import logics.Board;
import logics.Move;

import java.util.stream.Collectors;

// Accepts a board and the number of ms to think as arguments, chooses a move, applies the move to the board and prints the results
// TODO: Documentation of input and output format and parsing details
public class AgentCli {

    public static void main(String[] args) {

        Board board;
        int allowedMillis;

        try {
            board = new Board(args[0]);
            allowedMillis = Integer.parseInt(args[1]);
        }
        catch (Exception e) {
            System.out.println("error 1");
            return;
        }

        AgentMinimax agent = new AgentMinimax();

        agent.init(board, board.turn);

        agent.allowedMillis = allowedMillis;

        Move move = agent.chooseMove();

        board.move(move);

        String fen = board.getFen();

        String endresult;

        switch (board.getResult()) {
            case Board.RESULT_OPEN:
                endresult = "o";
                break;
            case Board.RESULT_WIN_WHITE:
                endresult = "w";
                break;
            case Board.RESULT_WIN_BLACK:
                endresult = "b";
                break;
            case Board.RESULT_TIE:
                endresult = "t";
                break;
            default:
                System.out.println("error 3");
                return;
        }

        System.out.println("fen: " + fen);
        System.out.println("move: " + move);
        System.out.println("moves: " + board.getMoves().stream().map(mv -> "\"" + mv.toString() + "\"").collect(Collectors.toList()).toString());
        System.out.println("endresult: " + endresult);
    }
}
