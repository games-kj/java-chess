package api;

import logics.Board;

import java.util.Arrays;
import java.util.stream.Collectors;

// Accepts a board as a parameter, prints the possible moves and status of the board
// TODO: Documentation of input and output format and parsing details
public class StatusCli {

    public static void main(String[] args) {

        Board board;

        try {
            board = new Board(args[0]);
        }
        catch (Exception e) {
            System.out.println("error 1");
            return;
        }

        String endresult;

        switch (board.getResult()) {
            case Board.RESULT_OPEN:
                endresult = "o";
                break;
            case Board.RESULT_WIN_WHITE:
                endresult = "w";
                break;
            case Board.RESULT_WIN_BLACK:
                endresult = "b";
                break;
            case Board.RESULT_TIE:
                endresult = "t";
                break;
            default:
                System.out.println("error 3");
                return;
        }

        System.out.println("moves: " + board.getMoves().stream().map(mv -> "\"" + mv.toString() + "\"").collect(Collectors.toList()).toString());
        System.out.println("endresult: " + endresult);
    }
}
